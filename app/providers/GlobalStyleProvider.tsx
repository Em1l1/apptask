"use client"

import React from 'react';

// styled 
import styled from 'styled-components'

interface Props {
  children: React.ReactNode;
}

function GlobalStyleProvider({ children }: Props) {

  const [isReady, setIsReady] = React.useState(false)

  React.useEffect(() => {
    setTimeout(() => {
      setIsReady(true)
    }, 200)

  }, [])

  if (!isReady) {
    return null;
  }

  return (
    <GlobalStyles>{children}</GlobalStyles>
  )
}

const GlobalStyles = styled.div`
  padding: 2.5rem;
  display: flex;
  gap: 2.5rem;
  height: 100%;
`;

export default GlobalStyleProvider
